#!/bin/bash

set -m

# https://github.com/kubernetes/kubernetes/issues/3595#issuecomment-354481790
# kubernetes rlimit support
ulimit -l unlimited

if [[ -z "${NODE_NAME}" ]]; then
	export NODE_NAME=$(hostname)
fi

export NODE_MASTER=${NODE_MASTER:-false}
export CA_FILE=/elasticsearch/config/searchguard/ssl/ca/root-ca.crt

# Add elasticsearch as command if needed
if [ "${1:0:1}" = '-' ]; then
	set -- elasticsearch "$@"
fi

if [[ ! -f "/elasticsearch/config/searchguard/ssl/${NODE_NAME}-signed.pem" ]]; then
	chown -R elasticsearch:elasticsearch /elasticsearch
	chown -R elasticsearch:elasticsearch /.backup
	if [[ "${NODE_MASTER}" = "true" ]]; then
        /run/miscellaneous/restore_config.sh
	fi
	/run/auth/certificates/gen_all.sh
fi

# Run as user "elasticsearch" if the command is "elasticsearch"
if [ "$1" = 'elasticsearch' -a "$(id -u)" = '0' ]; then
        chown -R elasticsearch:elasticsearch /elasticsearch
	set -- su-exec elasticsearch "$@"
	ES_JAVA_OPTS="-Des.network.host=$NETWORK_HOST -Des.logger.level=$LOG_LEVEL -Xms$HEAP_SIZE -Xmx$HEAP_SIZE"  $@ &
else
	$@ &
fi

if [[ "${NODE_MASTER}" = "true" ]]; then
	/run/miscellaneous/wait_until_started.sh
	/run/miscellaneous/index_level_settings.sh

	cat /elasticsearch/config/elasticsearch.yml

	/run/auth/users.sh
	/run/auth/sgadmin.sh
else
	cat /elasticsearch/config/elasticsearch.yml
fi

fg
